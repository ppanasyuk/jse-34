package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    Project removeProjectById(@NotNull String userId, @Nullable String projectId);

    Project removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    void clearProjects(@NotNull String userId);

    @NotNull
    Task unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

}