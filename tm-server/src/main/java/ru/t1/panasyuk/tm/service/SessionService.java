package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.service.ISessionService;
import ru.t1.panasyuk.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
    }

}