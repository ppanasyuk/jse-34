package ru.t1.panasyuk.tm.api.repository;

import ru.t1.panasyuk.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}